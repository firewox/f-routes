<?php


namespace Firewox\FRoutes\Exceptions;

class MismatchedGroupControllers extends \Exception
{

  public function __construct()
  {
    parent::__construct('Main and proxy group controllers are not the same.');
  }

}
