<?php

namespace Firewox\FRoutes;

use DI\Bridge\Slim\Bridge;
use DI\Container;
use Firewox\FRoutes\Utils\ClassFinder;

class Factory
{

  private Processor $processor;

  private function __construct(Container $container) {
    $this->processor = new Processor(Bridge::create($container));
  }

  public static function create(Container $container, string $rootDirectory, string $controllersNs, ?string $middlewareNs = null, array $middlewares = []): Processor {

    $factory = new Factory($container);

    /*
     * Get all the controller classes
     * recursively
     */

    $finder = new ClassFinder($rootDirectory);
    $classes = $finder->getClassesByNamespace($controllersNs);

    foreach ($classes as $className) {

      // Create reflection class
      $class = new \ReflectionClass($className);
      $factory->processor->ingestController($class);

    }

    /*
     * Get all the middleware classes
     * recursively
     */

    if ($middlewareNs) {

      $classes =  $finder->getClassesByNamespace($middlewareNs);

      foreach ($classes as $className) {

        // Create reflection class
        $class = new \ReflectionClass($className);
        $factory->processor->ingestApplicationMiddleware($class);

      }

    }


    /*
     * Merge middleware closures into list of processor middleware
     */

    foreach($middlewares as $priority => $callables)
      foreach($callables as $middleware)
        $factory->processor->addMiddleware($priority, $middleware, false);


    // Process the route handlers
    $factory->processor->process();

    return $factory->processor;

  }

}
