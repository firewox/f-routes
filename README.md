# F-Routes :sparkles:

This project has been created for the purpose of:

1. Simplifying creation of Slim routes
2. Making routes more readable and easier to understand

# Getting Started

1. Install composer package
2. Register the route processor
3. Add controllers
4. Add middleware


## Installation

```
composer require firewox/f-routes
```

## Registering Route Processor

Put the following code into your [index.php]():

```php
<?php

use DI\Container;
use Firewox\FRoutes\Factory;

$processor = Factory::create(
  new Container(),
  'Firewox\Tests\Controllers'       /* Namespace for controllers */
);

// Run the Slim app 
$processor->getApp()->run();

```

If you also have middleware for Slim you can include the middleware namespace as follows:

```php
<?php

use DI\Container;
use Firewox\FRoutes\Factory;

$processor = Factory::create(
  new Container(),
  'Firewox\Tests\Controllers',       /* Namespace for controllers */
  'Firewox\Tests\Middlewares'       /* Namespace for middlewares */
);

// Run the Slim app 
$processor->getApp()->run();

```

## Creating route controllers
Blah blah blah
1. Application routes
2. Group routes

## Creating middleware
Blah blah blah
1. Application middleware
2. Group middleware
3. Route middleware


## Attributes Documentation

### Controller Attribute

Property | Description | Possible Values
-------- | ----------- | ---------------
isGroup | Group controller or not. **(Default: [false]())** | [true]() or [false]()
pathPattern | Slim compliant route path. **(Default: '')** | *Any valid path*
middlewares | Slim middleware class names. Not necessarily marked with [Middleware]() attribute. **(Default: [])** | *0 or more class names*

### Route Attribute

Property | Description | Possible Values
-------- | ----------- | ---------------
methods | Valid slim HTTP verbs. **(Required)** | POST, PUT, GET, PATCH, DELETE, OPTIONS
pathPattern | Slim compliant route path. **(Required)** | *Any valid path*
name | Name of the route. **(Default: [null]())** | *Any valid name*
default | Application level default route or not. **(Default: [false]()). Ex. 404 default route. | [true]() or [false]()
priority | Order in which route should be registered. Multiple routes can have the same number. **(Default: 0)** | *Any number as needed*
middlewares | List of middleware class names. Not necessarily marked with [Middleware]() attribute. **(Default: [])** | *0 or more class names*
group | Controller class name. The controller class must be marked with [Controller]() attribute with [isGroup = true](). **(Default: [null]())** | *Valid class name*

### Middleware Attribute

Property | Description | Possible Values
-------- | ----------- | ---------------
priority | Order in which middleware should be registered. Multiple middleware can have the same number. **(Default: 0)** | *Any number as needed*
