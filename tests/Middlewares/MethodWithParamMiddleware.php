<?php


namespace Firewox\Tests\Middlewares;


use Firewox\FRoutes\Attributes\Middleware;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Request;

class MethodWithParamMiddleware
{

  public function __construct(private string $test)
  {
  }

  public function __invoke(Request $request, RequestHandlerInterface $handler) {

    $response = $handler->handle($request);
    $response->getBody()->write($this->test);
    return $response;

  }

}
