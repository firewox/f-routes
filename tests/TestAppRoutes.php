<?php

namespace Firewox\Tests;

use DI\Container;
use Firewox\FRoutes\Enums\HttpCode;
use Firewox\FRoutes\Enums\HttpMethod;
use Firewox\FRoutes\Factory;
use Firewox\FRoutes\Processor;
use Firewox\Tests\Traits\AppTestTrait;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Response;

class TestAppRoutes extends TestCase
{

  use AppTestTrait;

  public function testCreateSlimApp(): void {

    $this->assertInstanceOf(Processor::class, $this->processor);

  }

  public function testClassRouteHandler(): void {

    // Mock http request
    $request = $this->createRequest(HttpMethod::GET, '/invokable-class');
    $response = $this->processor->getApp()->handle($request);

    $this->assertSame(HttpCode::OK, $response->getStatusCode());

  }

  public function testClassDefaultRouteHandler(): void {

    // Mock http request
    $request = $this->createRequest(HttpMethod::GET, '/invokable-nonsense');
    $response = $this->processor->getApp()->handle($request);

    $this->assertSame(HttpCode::NOT_FOUND, $response->getStatusCode());

  }

  public function testClassRouteWithArgsHandler(): void {

    $testName = 'kuda';

    // Mock http request
    $request = $this->createRequest(HttpMethod::GET, '/invokable-class/' . $testName);
    $response = $this->processor->getApp()->handle($request);
    $response->getBody()->rewind();

    $this->assertSame(HttpCode::OK, $response->getStatusCode());
    $this->assertSame($testName . '__BEFORE_MIDDLEWARE____AFTER_MIDDLEWARE__',  $response->getBody()->getContents());

  }

  public function testMethodGetRouteHandler(): void {

    // Mock http request
    $request = $this->createRequest(HttpMethod::GET, '/invokable-method1');
    $response = $this->processor->getApp()->handle($request);

    $this->assertSame(HttpCode::OK, $response->getStatusCode());

  }

  public function testMethodPostRouteHandler(): void {

    // Mock http request
    $request = $this->createRequest(HttpMethod::POST, '/invokable-method2');
    $response = $this->processor->getApp()->handle($request);

    $this->assertSame(HttpCode::OK, $response->getStatusCode());

  }

  public function testMethodOptionsDefaultRouteHandler(): void {

    // Mock http request
    $request = $this->createRequest(HttpMethod::OPTIONS, '/invokable-method2');
    $response = $this->processor->getApp()->handle($request);

    $this->assertSame(HttpCode::OK, $response->getStatusCode());

  }

  public function testMethodGetRouteWithMiddlewareHandler(): void {

    // Mock http request
    $request = $this->createRequest(HttpMethod::GET, '/invokable-method4');
    $response = $this->processor->getApp()->handle($request);
    $response->getBody()->rewind();

    $this->assertSame(HttpCode::OK, $response->getStatusCode());
    $this->assertSame('__METHOD_MIDDLEWARE____BEFORE_MIDDLEWARE____AFTER_MIDDLEWARE__',  (string)$response->getBody());

  }

  public function testMethodGetRouteWithParamMiddlewareHandler(): void {

    // Mock http request
    $request = $this->createRequest(HttpMethod::GET, '/invokable-method5');
    $response = $this->processor->getApp()->handle($request);
    $response->getBody()->rewind();

    $this->assertSame(HttpCode::OK, $response->getStatusCode());
    $this->assertSame('__PARAM____BEFORE_MIDDLEWARE____AFTER_MIDDLEWARE__',  (string)$response->getBody());

  }

  public function testMethodGetRouteWithCallableMiddlewareHandler(): void {

    $fn = function(ServerRequestInterface $request, RequestHandlerInterface $handler) {
      $response = $handler->handle($request);
      $body = (string)$response->getBody();
      $response = new Response();
      $response->getBody()->write("($body)");
      return $response;
    };

    $processor = Factory::create(
      new Container(),
      __DIR__ . '/../',
      'Firewox\Tests\Controllers',
      'Firewox\Tests\Middlewares',
      [
        1 => [ $fn ]
      ]
    );

    // Mock http request
    $request = $this->createRequest(HttpMethod::GET, '/invokable-method4');
    $response = $processor->getApp()->handle($request);
    $response->getBody()->rewind();

    $this->assertSame(HttpCode::OK, $response->getStatusCode());
    $this->assertSame('(__METHOD_MIDDLEWARE____BEFORE_MIDDLEWARE____AFTER_MIDDLEWARE__)',  (string)$response->getBody());

  }

}
