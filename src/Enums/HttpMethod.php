<?php


namespace Firewox\FRoutes\Enums;


final class HttpMethod
{
  public const POST = 'POST';
  public const GET = 'GET';
  public const DELETE = 'DELETE';
  public const PATCH = 'PATCH';
  public const OPTIONS = 'OPTIONS';
  public const PUT = 'PUT';
  public const TRACE = 'TRACE';
  public const CONNECT = 'CONNECT';
  public const HEAD = 'HEAD';
}
