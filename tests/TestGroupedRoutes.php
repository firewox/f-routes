<?php

namespace Firewox\Tests;

use Firewox\FRoutes\Processor;
use Firewox\Tests\Traits\AppTestTrait;
use PHPUnit\Framework\TestCase;

class TestGroupedRoutes extends TestCase
{

  use AppTestTrait;

  public function testMethodGetRouteHandler(): void {

    // Mock http request
    $request = $this->createRequest('GET', '/grouped/invokable-method1');
    $response = $this->processor->getApp()->handle($request);

    $this->assertSame(200, $response->getStatusCode());

  }

  public function testMethodPostRouteHandler(): void {

    // Mock http request
    $request = $this->createRequest('POST', '/grouped/invokable-method2');
    $response = $this->processor->getApp()->handle($request);

    $this->assertSame(200, $response->getStatusCode());

  }

  public function testMethodGetRouteHandler2(): void {

    // Mock http request
    $request = $this->createRequest('GET', '/grouped/invokable-method3');
    $response = $this->processor->getApp()->handle($request);

    $this->assertSame(200, $response->getStatusCode());

  }

}
