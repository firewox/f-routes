<?php


namespace Firewox\FRoutes\Attributes;


#[\Attribute]
class Route
{

  /**
   * Route constructor.
   * @param string[]|string $methods
   * @param string $pathPattern
   * @param string|null $name
   * @param bool $default
   * @param int $priority
   * @param array $middlewares
   * @param string|null $group
   */
  public function __construct(public array|string $methods,
                              public string $pathPattern,
                              public ?string $name = null,
                              public bool $default = false,
                              public int $priority = 0,
                              public array $middlewares = [],
                              public ?string $group = null) {}

}
