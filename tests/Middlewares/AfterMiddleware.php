<?php


namespace Firewox\Tests\Middlewares;


use Firewox\FRoutes\Attributes\Middleware;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Request;

#[Middleware(priority: 1)]
class AfterMiddleware
{

  public function __invoke(Request $request, RequestHandlerInterface $handler) {

    $response = $handler->handle($request);
    $response->getBody()->write('__AFTER_MIDDLEWARE__');
    return $response;

  }

}
