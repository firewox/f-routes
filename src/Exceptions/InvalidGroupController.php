<?php


namespace Firewox\FRoutes\Exceptions;

class InvalidGroupController extends \Exception
{

  public function __construct(string $className)
  {
    parent::__construct("Class missing controller attribute cannot be used as a group controller: ({$className})");
  }

}
