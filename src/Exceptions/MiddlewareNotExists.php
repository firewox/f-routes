<?php


namespace Firewox\FRoutes\Exceptions;

class MiddlewareNotExists extends \Exception
{

  public function __construct(string $className)
  {
    parent::__construct("Middleware does not exist: ({$className})");
  }

}
