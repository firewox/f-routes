<?php


namespace Firewox\FRoutes;


use Firewox\FRoutes\Attributes\Controller;
use Firewox\FRoutes\Attributes\Middleware;
use Firewox\FRoutes\Attributes\Route;
use Firewox\FRoutes\Exceptions\ClassNotExists;
use Firewox\FRoutes\Exceptions\InvalidGroupController;
use Firewox\FRoutes\Exceptions\InvalidMiddleware;
use Firewox\FRoutes\Exceptions\MiddlewareNotExists;
use Firewox\FRoutes\Exceptions\MismatchedGroupControllers;
use Slim\App;
use Slim\Interfaces\RouteGroupInterface;
use Slim\Interfaces\RouteInterface;
use Slim\Routing\RouteCollectorProxy;

class Processor
{

  private array $groups = [];
  private array $generalHandlers = [];
  private array $defaultHandlers = [];
  private array $middlewares = [];

  public function __construct(private App $app) { }


  /**
   * @return App
   */
  public function getApp(): App
  {
    return $this->app;
  }


  public function ingestApplicationMiddleware(\ReflectionClass $class): void
  {

    // Check if controller class
    $middlewareAttributes = $class->getAttributes(Middleware::class);

    if (count($middlewareAttributes) > 0) {

      /* @var Middleware $middleware */
      $middleware = $middlewareAttributes[0]->newInstance();
      $this->middlewares[$middleware->priority][] = $class->getName();

    }

  }


  public function ingestController(\ReflectionClass $class): void
  {

    // Check if controller class
    $classAttributes = $class->getAttributes(Controller::class);
    $routeAttributes = $class->getAttributes(Route::class);

    if (count($classAttributes) > 0) {

      // Get all the class methods
      $methods = $class->getMethods();

      foreach($methods as $method) {

        // Check if method is annotated or not
        $methodAttributes = $method->getAttributes(Route::class);
        if (count($methodAttributes) === 0) continue;

        // Process first route attribute only
        $this->ingestMethodRouteAttribute($class, $method, $classAttributes[0], $methodAttributes[0]);

      }

    } else if (count($routeAttributes) > 0) {

      $this->ingestClassRouteAttribute($class, $routeAttributes[0]);

    }


  }


  private function generateHttpMethods(Route $route): array {
    return is_array($route->methods) ? $route->methods : [$route->methods];
  }


  private function cacheFn(?Controller $groupController, Route $route, callable $fn): void {

    if ($route->default) {
      $this->defaultHandlers[$route->priority][] = $fn;
      return;
    }

    $groupName = $route->group ?: '_';
    $this->generalHandlers[$groupName][$route->priority][] = [$groupController, $route, $fn];
    $this->groups[$groupName] = $groupController;

  }


  private function getGroupControllerFromRoute(Route $route): ?Controller {

    // Check class exists
    if (!class_exists($route->group))
      throw new ClassNotExists($route->group);

    // Load group class
    $class = new \ReflectionClass($route->group);

    // Check if its annotated with controller attribute
    $classAttributes = $class->getAttributes(Controller::class);
    if (count($classAttributes) === 0)
      throw new InvalidGroupController($route->group);

    // Return group controller
    /* @var Controller $controller */
    $controller = $classAttributes[0]->newInstance();

    return $controller;

  }


  private function ingestMethodRouteAttribute(\ReflectionClass $class,
                                              \ReflectionMethod $method,
                                              \ReflectionAttribute $controllerAttribute,
                                              \ReflectionAttribute $routeAttribute)
  {

    /* @var Route $route */
    /* @var Controller $controller */

    $route = $routeAttribute->newInstance();
    $controller = $controllerAttribute->newInstance();
    $route->group = !$route->group && $controller->isGroup ? $class->getName() : $route->group;
    $groupController = $route->group ? $this->getGroupControllerFromRoute($route) : null;

    $mapFn = function(Processor $obj, App|RouteInterface|RouteCollectorProxy $slim)
    use($route, $class, $method) {
      $proxy = $slim->map(
        $obj->generateHttpMethods($route),
        $route->pathPattern,
        [$class->getName(), $method->getName()]
      );
      if ($route->name) $proxy->setName($route->name);
      $obj->registerMiddlewares($proxy, $route->middlewares);
    };

    $this->cacheFn($groupController, $route, $mapFn);

  }


  private function ingestClassRouteAttribute(\ReflectionClass $class, \ReflectionAttribute $routeAttribute)
  {

    /* @var Route $route */
    $route = $routeAttribute->newInstance();
    $controller = $route->group ? $this->getGroupControllerFromRoute($route) : null;

    $mapFn = function(Processor $obj, App|RouteInterface|RouteCollectorProxy $slim) use($route, $class) {
      $proxy = $slim->map(
        $obj->generateHttpMethods($route),
        $route->pathPattern,
        $class->getName()
      );
      if ($route->name) $proxy->setName($route->name);
      $obj->registerMiddlewares($proxy, $route->middlewares);
    };

    $this->cacheFn($controller, $route, $mapFn);

  }


  public function process(): void {

    // Sort by priority
    ksort($this->middlewares);

    /*
     * Register application middlewares
     */
    foreach ($this->middlewares as $priority => $middlewares)
      $this->registerMiddlewares($this->getApp(), $middlewares);


    // General route handlers
    foreach($this->generalHandlers as $group => $val) {

      /* @var Controller $mainGroupController */
      $mainGroupController =  $this->groups[$group];

      if ($mainGroupController) {

        /*
         * Register grouped routes
         */
        $slimGroup = $this->getApp()->group(
          $mainGroupController->pathPattern,
          fn (RouteCollectorProxy $groupProxy) =>
            $this->registerRouteHandlers($groupProxy, $mainGroupController, $group)
        );

        /*
         * Register group middlewares
         */
        $this->registerMiddlewares($slimGroup, $mainGroupController->middlewares);

      } else {

        /*
         * Register standalone routes
         */
        $this->registerRouteHandlers($this->getApp(), null, $group);

      }

    }

      // Default route handlers
    foreach($this->defaultHandlers as $handlers)
      foreach ($handlers as $handler)
        $handler($this, $this->getApp());

  }


  private function registerRouteHandlers(App|RouteCollectorProxy $proxy, ?Controller $mainGroupController, string $group): void {

    $handlers = $this->generalHandlers[$group];
    ksort($handlers);

    foreach ($handlers as $priority => $priorities)
    foreach ($priorities as [$groupController, $route, $fn]){

      /* @var Controller $groupController */
      /* @var Route $route */
      /* @var callable $fn */
      if (($groupController && !$mainGroupController) || (!$groupController && $mainGroupController))
        throw new MismatchedGroupControllers();
      if (
        $groupController &&
        $mainGroupController &&
        $groupController->pathPattern !== $mainGroupController->pathPattern
      )
        throw new MismatchedGroupControllers();

      // Register route
      $fn($this, $proxy);

    }

  }


  private function registerMiddlewares(App|RouteGroupInterface|RouteCollectorProxy|RouteInterface $slim,
                                       array $middlewares): void {

    foreach($middlewares as $key => $middleware) {

      if (is_string($middleware)) {

        if(!class_exists($middleware)) throw new MiddlewareNotExists($middleware);
        $slim->add($this->getApp()->getContainer()->get($middleware));

      } else if (is_array($middleware)) {

        if(!class_exists($key)) throw new MiddlewareNotExists($key);
        $slim->add($this->getApp()->getContainer()->make($key, $middleware));

      } else if (is_callable($middleware) || is_object($middleware)) {

        $slim->add($middleware);

      } else {

        throw new InvalidMiddleware(json_encode($middleware));

      }

    }

  }


  public function addMiddleware(int $priority, callable|object $middleware, bool $register = true)
  {
    $this->middlewares[$priority][] = $middleware;
    if ($register) $this->registerMiddlewares($this->getApp(), [$middleware]);
  }


}
