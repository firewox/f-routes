<?php


namespace Firewox\FRoutes\Utils;


final class ClassFinder
{

  private static $composer = null;
  private static $classes  = [];

  public function __construct(string $appRootPath)
  {
    self::$composer = null;
    self::$classes  = [];

    self::$composer = require $appRootPath.'vendor/autoload.php';

    if (false === empty(self::$composer)) {
      self::$classes  = array_keys(self::$composer->getClassMap());
    }
  }

  public function getClasses()
  {
    $allClasses = [];

    if (false === empty(self::$classes)) {
      foreach (self::$classes as $class) {
        $allClasses[] = '\\' . $class;
      }
    }

    return $allClasses;
  }

  public function getClassesByNamespace($namespace)
  {
    if (0 !== strpos($namespace, '\\')) {
      $namespace = '\\' . $namespace;
    }

    $termUpper = strtoupper($namespace);
    return array_filter($this->getClasses(), function($class) use ($termUpper) {
      $className = strtoupper($class);
      if (
        str_starts_with($className, $termUpper) and
        !str_contains($className, strtoupper('Abstract')) and
        !str_contains($className, strtoupper('Interface'))
      ){
        return $class;
      }
      return false;
    });
  }

  public function getClassesWithTerm($term)
  {
    $termUpper = strtoupper($term);
    return array_filter($this->getClasses(), function($class) use ($termUpper) {
      $className = strtoupper($class);
      if (
        str_contains($className, $termUpper) and
        !str_contains($className, strtoupper('Abstract')) and
        !str_contains($className, strtoupper('Interface'))
      ){
        return $class;
      }
      return false;
    });
  }

}
