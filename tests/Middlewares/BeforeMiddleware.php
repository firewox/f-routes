<?php


namespace Firewox\Tests\Middlewares;


use Firewox\FRoutes\Attributes\Middleware;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Request;

#[Middleware(priority: 0)]
class BeforeMiddleware
{

  public function __invoke(Request $request, RequestHandlerInterface $handler) {

    $response = $handler->handle($request);
    $response->getBody()->write('__BEFORE_MIDDLEWARE__');
    return $response;

  }

}
