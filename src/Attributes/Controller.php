<?php


namespace Firewox\FRoutes\Attributes;

#[\Attribute]
class Controller
{

  /**
   * Controller constructor.
   * @param bool $isGroup
   * @param string $pathPattern
   * @param array $middlewares
   */
  public function __construct(public bool $isGroup = false,
                              public string $pathPattern = '',
                              public array $middlewares = []) {}

}
