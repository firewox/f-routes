<?php

namespace Firewox\Tests\Controllers;

use Firewox\FRoutes\Attributes\Controller;
use Firewox\FRoutes\Attributes\Route;
use Firewox\FRoutes\Enums\HttpCode;
use Firewox\FRoutes\Enums\HttpMethod;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

#[Controller(isGroup: true, pathPattern: '/grouped')]
class MethodGroupedRouteController
{

  public function __construct(private ContainerInterface $container) { }

  #[Route([HttpMethod::GET], '/invokable-method1')]
  public function handler1(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
  {
    return $response->withStatus(HttpCode::OK);
  }

  #[Route(HttpMethod::POST, '/invokable-method2')]
  public function handler2(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
  {
    return $response->withStatus(HttpCode::OK);
  }

}
