<?php


namespace Firewox\FRoutes\Attributes;


#[\Attribute]
class Middleware
{

  /**
   * Application middleware constructor.
   * @param int $priority
   */
  public function __construct(public int $priority = 0) {}

}
