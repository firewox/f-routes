<?php

namespace Firewox\Tests\Controllers;

use Firewox\FRoutes\Attributes\Controller;
use Firewox\FRoutes\Attributes\Route;
use Firewox\FRoutes\Enums\HttpCode;
use Firewox\FRoutes\Enums\HttpMethod;
use Firewox\Tests\Middlewares\MethodMiddleware;
use Firewox\Tests\Middlewares\MethodWithParamMiddleware;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

#[Controller]
class MethodRouteController
{

  public function __construct(private ContainerInterface $container) { }

  #[Route([HttpMethod::GET], '/invokable-method1')]
  public function handler1(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
  {
    return $response->withStatus(HttpCode::OK);
  }

  #[Route(HttpMethod::POST, '/invokable-method2')]
  public function handler2(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
  {
    return $response->withStatus(HttpCode::OK);
  }

  #[Route(methods: HttpMethod::GET, pathPattern: '/invokable-method3', group: MethodGroupedRouteController::class)]
  public function handler3(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
  {
    return $response->withStatus(HttpCode::OK);
  }

  #[Route(methods: HttpMethod::GET, pathPattern: '/invokable-method4', middlewares: [MethodMiddleware::class])]
  public function handler4(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
  {
    return $response->withStatus(HttpCode::OK);
  }

  #[Route(methods: HttpMethod::OPTIONS, pathPattern: '{routes:.+}', default: true)]
  public function handler5(ResponseInterface $response): ResponseInterface
  {
    return $response->withStatus(HttpCode::OK);
  }

  #[Route(methods: HttpMethod::GET,
    pathPattern: '/invokable-method5',
    middlewares: [MethodWithParamMiddleware::class => ['test' => '__PARAM__'] ])
  ]
  public function handler6(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
  {
    return $response->withStatus(HttpCode::OK);
  }

}
