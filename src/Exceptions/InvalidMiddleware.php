<?php


namespace Firewox\FRoutes\Exceptions;

class InvalidMiddleware extends \Exception
{

  public function __construct(string $details)
  {
    parent::__construct("Invalid middleware found: ({$details})");
  }

}
