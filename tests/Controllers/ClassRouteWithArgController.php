<?php

namespace Firewox\Tests\Controllers;

use Firewox\FRoutes\Attributes\Route;
use Firewox\FRoutes\Enums\HttpCode;
use Firewox\FRoutes\Enums\HttpMethod;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

#[Route(HttpMethod::GET, '/invokable-class/{name}')]
class ClassRouteWithArgController
{

  public function __construct(private ContainerInterface $container) { }

  public function __invoke(ServerRequestInterface $request, ResponseInterface $response, string $name): ResponseInterface
  {
    $response->getBody()->write($name);
    return $response->withStatus(HttpCode::OK);
  }

}
