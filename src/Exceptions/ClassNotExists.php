<?php


namespace Firewox\FRoutes\Exceptions;

class ClassNotExists extends \Exception
{

  public function __construct(string $className)
  {
    parent::__construct("Class does not exist: ({$className})");
  }

}
